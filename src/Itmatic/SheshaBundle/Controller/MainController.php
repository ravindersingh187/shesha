<?php

namespace Itmatic\SheshaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use JMS\Serializer\SerializationContext;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class MainController extends FOSRestController {

    /**
     * @Rest\Post("/",name="add")
     * @Rest\View()
     * 
     */
    public function addAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $username = $request->get('username', null);
        $email = $request->get('email', null);
        $password = $request->get('password', null);
        $date = new \DateTime();

        if (!$username) {
            throw new HttpException(404, 'not found');
        }

        $user = new \Itmatic\SheshaBundle\Entity\User();
        $user->setUserName($username);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setCreatedAt($date);
        $em->persist($user);
        $em->flush();
        return $this->handleView($this->view($user));
    }

    /**
     * @Rest\Post("/login",name="login")
     * @Rest\View()
     * 
     */
    public function loginAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $username = $request->get('username', null);
        $password = $request->get('password', null);

        $result = $em->getRepository('ItmaticSheshaBundle:User')
                ->findOneBy(array('userName' => $username,
            'password' => $password));
        if (!$result) {
            throw new HttpException(404, 'not found');
        }
        return $this->handleView($this->view($result));
    }

    /**
     * @Rest\Post("/add",name="addphoto")
     * @Rest\View()
     * 
     */
    public function addphotoAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $title = $request->get('username', null);
        $image = $request->files->get('image');
        $view = '0';
        $like = '0';
        $date = new \DateTime();
        $photo = new \Itmatic\SheshaBundle\Entity\Photo();
        $photo->setTitle($title);
        $photo->setLikes($like);
        $photo->setView($view);
        $photo->setFile($image);
        $photo->setCreatedAt($date);
        $photo->setExtension('.jpg');
        $em->persist($photo);
        $em->flush();
        return $this->handleView($this->view($photo));
    }

    /**
     * @Rest\Post("/like/{id}",name="like")
     * @Rest\View()
     * 
     */
    public function likeAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $obj = $em->getRepository('ItmaticSheshaBundle:Photo')
                ->find($id);

        $likes = $obj->getLikes();
        $update_likes = $likes + 1;

        $obj->setLikes($update_likes);
        $em->persist($obj);
        $em->flush();
        return $this->handleView($this->view($obj));
    }

    /**
     * @Rest\Post("/view/{id}",name="view")
     * @Rest\View()
     * 
     */
    public function viewAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $obj = $em->getRepository('ItmaticSheshaBundle:Photo')
                ->find($id);

        $view = $obj->getView();
        $update_view = $view + 1;

        $obj->setView($update_view);
        $em->persist($obj);
        $em->flush();
        return $this->handleView($this->view($obj));
    }

}
