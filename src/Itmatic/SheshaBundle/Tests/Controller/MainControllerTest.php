<?php

namespace Itmatic\SheshaBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testUser()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user');
    }

}
