<?php

namespace Itmatic\SheshaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use JMS\Serializer\Annotation\Accessor;

/**
 * Photo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Itmatic\SheshaBundle\Entity\PhotoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Photo {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="view", type="string", length=255)
     */
    private $view;

    /**
     * @var string
     *
     * @ORM\Column(name="likes", type="string", length=255)
     */
    private $likes;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", length=255, nullable=true)
     */
    private $extension;
    private $file;

    /**
     * @var string $icon_url
     * @Accessor(getter="getIconUrl",setter="setIconUrl")
     */
    private $icon_url = false;

    public function getIconUrl() {
        if ($this->icon_url !== false)
            return $this->icon_url;
        if ($this->file == '')
            return false;
        return 'http://192.168.2.106/uploads/catIcons/' . $this->getId().'.'.getExtension();
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setFile($file) {
        $this->file = $file;


        return $this;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/images';
    }

    public function moveIcon() {
        $dir = $this->getUploadRootDir();
        $filename = $this->getId() . '.' . $this->getExtension();
        $this->file->move($dir, $filename);
    }

    /**
     * 
     * @ORM\PostPersist()
     */
    public function postPersist() {

        $this->moveIcon();
    }

    /**
     * 
     * @ORM\PostUpdate()
     */
    public function postUpdate() {

        $this->moveIcon();
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created_at;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Photo
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set view
     *
     * @param string $view
     * @return Photo
     */
    public function setView($view) {
        $this->view = $view;

        return $this;
    }

    /**
     * Get view
     *
     * @return string 
     */
    public function getView() {
        return $this->view;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Photo
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return Photo
     */
    public function setExtension($extension) {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string 
     */
    public function getExtension() {
        return $this->extension;
    }

    /**
     * Set likes
     *
     * @param string $likes
     * @return Photo
     */
    public function setLikes($likes) {
        $this->likes = $likes;

        return $this;
    }

    /**
     * Get likes
     *
     * @return string 
     */
    public function getLikes() {
        return $this->likes;
    }

}
